import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { infoPagina } from '../interfaces/info-pagina.interface';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {
  info: infoPagina[] = [];
  cargada = false;

  constructor( private http: HttpClient) {
    console.log('servicios funcionando');

/* se utiliza el metodo get para obtener la información de la base de datos 
 de FireBase de Google. 
Luego se hace una cast para que los datos sean tipo "infoPagina" el cual se creo la interfaz 
para leer los atributos del objeto.
 */
    this.http.get('https://pruebaand-bfd7f.firebaseio.com/lista.json')
    .subscribe( (resp: infoPagina[]) => {
      this.cargada = true;
      this.info = resp;
      console.log(resp);
    });
   }
}
