
/* Interfaz para mapear el objeto de la base de datos */
export interface infoPagina {
    cod?: string;
    url?: string;
    class?: string;
    fecha?: string;
    descripcion?: string;
  }