import { Component, OnInit } from '@angular/core';
import { InfoPaginaService } from '../../services/info-pagina.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  /* Se inyecta el servicio InfoPaginaService para utilizarlo en el HTML de la pagina principal */

  constructor( public infoPaginaService: InfoPaginaService) { }

  ngOnInit(): void {
  }

}
