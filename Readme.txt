Este proyecto fue realizado con Angular.
Editor de texto utilizado:
Visual Studio Code

Instalar Node.JS, si no lo ha instalado previamente para ejecutar los proyectos de Angular:
https://nodejs.org/es/ 

Si no ha instalado Angular ejecutar el siguiente comando en el cmd:
npm install -g @angular/cli

Para ejecutar el proyecto, dirijase a la carpeta raiz y en esa ubicación ejecute en el cmd:
ng serve

En el navegador de su preferencia escriba:
http://localhost:4200/

El proyecto tiene una base de datos con Firebase de Google el cual se puede acceder mediante una invitación, que le hice llegar previamente.

Se crea un archivo Json el cual, se importa en Firebase donde se puede realizar el mapeo de los datos. Puede consultar este archivo de forma local en la siguiente ubicación:

/assets/data/pruebaAND.json

Asì mismo se almacenan las imagenes y se añaden las URL a la base de datos. Esta conectado todo en tiempo real.

Para la creación de la página se tuvo en cuenta los elementos comunes como lo son el Header y el Footer. Para esto se crearon sus respectivos componentes en la carpeta "shared". 

Para la comunicación de los datos, se ha creado un servicio llamado "info-pagina.service.ts" el cual através del metodo Get, obtiene los datos de la base de datos. En el "app.module.ts" agregar en las importaciones "HttpClienteModule" el cual permitirá utilizar los verbos http.

Para interactuar con la información de Firebase, se implementa una interfaz que permite realizar el mapeo y utilizar las variables de una forma más sencilla.

Se crea una carpeta llamada "Pages", en la cual se almacenarán todas las páginas del proyecto.

Las imagenes que fueron tratadas localmente se encuentran en:
/assets/img
